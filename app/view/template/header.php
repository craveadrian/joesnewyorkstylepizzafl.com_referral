<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop resSection">
				<div class="row">
					<div class="hdTop">
						<div class="hdTopLeft col-2 fl resCenter">
							<p class="label">FOLLOW US</p>
							<p>
								<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
								<a href="<?php $this->info("gp_link"); ?>" class="socialico">g</a>
								<a href="<?php $this->info("li_link"); ?>" class="socialico">i</a>
								<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
							</p>
						</div>
						<div class="hdTopMid col-8 fl resCenter">
							<p><?php $this->info("address"); ?></p>
						</div>
						<div class="hdTopRight col-2 fr resCenter">
							<p class="label">CALL US AT</p>
							<p><?php $this->info(["phone","tel"]); ?></p>
							<p class="label">FAX</p>
							<p><?php $this->info(["fax","tel"]); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul >
							<div class="navLeft col-6 fl">
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
							</div>
							<div class="navRight col-6 fr">
								<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
								<li <?php $this->helpers->isActiveMenu("menu"); ?>><a href="<?php echo URL ?>menu">MENU</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
							</div>
							<div class="clearfix"></div>
						</ul>
					</nav>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner" class="resSection">
			<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Logo" class="mainLogo"/> </a>
			<div class="row">
				<div class="banLeft col-4 fl resCenter">
					<p class="capture">THE INNOVATOR OF OUR FAMOUS</p>
					<p class="abril">Buffalo Chicken Deep Dish Pizza</p>
				</div>
				<div class="banRight col-4 fr resCenter">
					<p class="abril">Catering Available</p>
					<p class="capture">All our Pies are made with “Grande Cheese”</p>
					<p class="capture">Our Pies are Hand Stretched and Cooked Directly on the Brick</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	<?php endif; ?>
