<div id="content" class="resSection">
	<div class="row">
		<h1>MENU</h1>
		<div id="gall2" class="gallery-container">
			<ul class="gallery menu clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "menu") { ?>
				<li>
					<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
						<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
					</a>
				</li>
				<?php }} ?>
			</ul>
			<div class="page_navigation"></div>
		</div>
	</div>
</div>
