<div id="content">
	<div id="offerSec" class="resSection">
		<div class="row">
			<h1>WHAT WE OFFER</h1>
			<p>Our highly experienced team has what it takes to fulfill all aspects of your craving needs. We are known for our punctual and respectful service, and we give all of our clients the personalized catering they deserve. Our services include:</p>
			<div class="offers">
				<dl>
					<dt> <img src="public/images/place.png" alt="Lunch" class="bg-lunch"> </dt>
					<dd>Lunch</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/place.png" alt="Dinner" class="bg-dinner"> </dt>
					<dd>Dinner</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/place.png" alt="Desserts" class="bg-desserts"> </dt>
					<dd>Desserts</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/place.png" alt="Drinks" class="bg-drinks"> </dt>
					<dd>Drinks</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/place.png" alt="Specials" class="bg-specials"> </dt>
					<dd>Specials</dd>
				</dl>
			</div>
			<a href="<?php echo URL ?>contact#content" class="order">ORDER ONLINE</a>
		</div>
	</div>
	<div id="menuSec" class="resSection">
		<div class="row">
			<h1>OUR MENU</h1>
			<div class="menuTop">
				<dl>
					<dt>NY Style Pizza</dt>
					<dd>
						<img src="public/images/content/menu1.jpg" alt="NY Style Pizza">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Stuffed Pizzas</dt>
					<dd>
						<img src="public/images/content/menu2.jpg" alt="Stuffed Pizzas">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Sausage Rolls</dt>
					<dd>
						<img src="public/images/content/menu3.jpg" alt="Sausage Rolls">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Pasta</dt>
					<dd>
						<img src="public/images/content/menu4.jpg" alt="Pasta">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Cols Subs</dt>
					<dd>
						<img src="public/images/content/menu5.jpg" alt="Cols Subs">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Salads</dt>
					<dd>
						<img src="public/images/content/menu6.jpg" alt="Salads">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Tacos</dt>
					<dd>
						<img src="public/images/content/menu7.jpg" alt="Tacos">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
				<dl>
					<dt>Burgers</dt>
					<dd>
						<img src="public/images/content/menu8.jpg" alt="Burgers">
						<div class="bot">
							<a href="<?php echo URL ?>menu#content" class="btn">VIEW MORE</a>
						</div>
					</dd>
				</dl>
			</div>
			<img src="public/images/content/img1.png" alt="spices" class="resImg img1">
		</div>
	</div>
	<div id="gallerySec" class="resSection">
		<div class="row">
			<div class="gLeft col-4 fl resSection">
				<img src="public/images/content/feature.jpg" alt="Feature" class="feature"/>
				<h4>welcome to</h4>
				<h1>JOE'S</h1>
				<h3>NY Style Pizzeria & Restaurant</h3>
				<p>It had been a long time since i’d taken the Grant Street exit off the 198. But when i did so the other day and came up to the intersection of Grant and Amherst, I noticed a neew business tucked in the corner of the strip plaza, in the spot once occupied by Mighty Taco, When i saw the name Joe’s New York Style Pizzeria & Restaurant my first reaction was skepticism, New York Style” is such a bold declaration, promising to distinguish the product from other local pizzerias in some special way, But being a pizza junkie, I had to give their pizza a try, And I am pleased to report  that the two pepperoni slices i tried were delicious,...</p>
				<a href="<?php echo URL?>about#content" class="btn">Learn More</a>
				<img src="public/images/content/img2.png" alt="spices" class="spices resImg"/>
			</div>
			<div class="gRight col-5 fr resSection">
				<h1>OUR GALLERY</h1>
				<div class="lgImg">
					<img src="public/images/content/gallery1.jpg" alt="Gallery1">
					<a href="<?php echo URL ?>gallery#content" class="btngall resPos">VIEW OUR<br>GALLERY</a>
				</div>
				<div class="smImg">
					<img src="public/images/content/gallerySm1.jpg" alt="Small 1">
					<img src="public/images/content/gallerySm2.jpg" alt="Small 2">
					<img src="public/images/content/gallerySm3.jpg" alt="Small 3">
					<img src="public/images/content/gallerySm4.jpg" alt="Small 4">
					<img src="public/images/content/gallerySm5.jpg" alt="Small 5">
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="reviewSec" class="resSection">
		<div class="row">
			<h1>REVIEWS</h1>
			<div class="reviews">
				<dl class="col-4">
					<dt><img src="public/images/content/rvw1.jpg" alt="Review Image 1" /></dt>
					<dd>
						<p>&#9733; &#9733; &#9733; &#9733; &#9733;</p>
						<p>-Jojo B.</p>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud.</p>
					</dd>
				</dl>
				<dl class="col-4">
					<dt><img src="public/images/content/rvw2.jpg" alt="Review Image 2" /></dt>
					<dd>
						<p>&#9733; &#9733; &#9733; &#9733; &#9733;</p>
						<p>-Carl B.</p>
						<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent.</p>
					</dd>
				</dl>
				<dl class="col-4">
					<dt><img src="public/images/content/rvw3.jpg" alt="Review Image 3" /></dt>
					<dd>
						<p>&#9733; &#9733; &#9733; &#9733; &#9733;</p>
						<p>-Erick G.</p>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,</p>
					</dd>
				</dl>
			</div>
			<a href="<?php echo URL ?>reviews#content" class="btn">VIEW MORE</a>
		</div>
	</div>
	<div id="qContactSec" class="resSection">
		<div class="row">
			<div class="quickform">
				<h1>CONTACT US</h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<div class="formTop">
						<label class="col-4"><span class="ctc-hide">Name</span>
							<input type="text" name="name" placeholder="Name:">
						</label>
						<label class="col-4"><span class="ctc-hide">Email</span>
							<input type="text" name="email" placeholder="Email:">
						</label>
						<label class="col-4"><span class="ctc-hide">Phone</span>
							<input type="text" name="phone" placeholder="Phone:">
						</label>
					</div>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
				</form>
			</div>
			<img src="public/images/content/img3.png" alt="vegetables" class="vegetables resImg">
		</div>
	</div>
</div>
